package com.epam.shvaiba.validator;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ValidatorTest {


    private Validator validator = new Validator();
    @Test
    public void validateRowTrue() {
        String str =  "QA Alex Saint B1 Manual 40";
        assertTrue(validator.validateRow(str));
    }

    @Test
    void validateFalse() {
        String str =  "QB Alex Saint B1 Manual 40";
        assertFalse (validator.validateRow(str));
    }
}
