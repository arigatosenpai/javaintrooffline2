package com.epam.shvaiba;

import com.epam.shvaiba.factory.EmployeeFactory;
import com.epam.shvaiba.model.Employee;


import java.util.ArrayList;
import java.util.List;

public class TestHelper {
    private List<Employee> testEmployees = new ArrayList<>();

    private void testDataCreation() {
        EmployeeFactory ef = new EmployeeFactory();
        testEmployees.add(ef.createEmployee(new String[]{"ba", "Ilya", "Shevchenka", "C2", "10"}));
        testEmployees.add(ef.createEmployee(new String[]{"qa", "Alex", "Saint", "B1", "Manual", "90"}));
        testEmployees.add(ef.createEmployee(new String[]{"developer", "Alex", "Ivanov", "B2", "Java", "80"}));
        testEmployees.add(ef.createEmployee(new String[]{"ba", "Alex", "Kulikou", "B2", "80"}));
        testEmployees.add(ef.createEmployee(new String[]{"qa", "Ilya", "Averkov", "B2", "Manual", "80"}));
        testEmployees.add(ef.createEmployee(new String[]{"developer", "Andrei", "Ivanov", "B2", "Java", "80"}));
    }

    public List<Employee> getTestEmployee() {
        if (testEmployees.isEmpty()) {
                testDataCreation();
            return testEmployees;
        }
        else{
            return  testEmployees;
        }
    }
}
