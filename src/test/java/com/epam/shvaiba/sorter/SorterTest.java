package com.epam.shvaiba.sorter;

import com.epam.shvaiba.TestHelper;
import com.epam.shvaiba.factory.EmployeeFactory;
import com.epam.shvaiba.model.Employee;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.*;

public class SorterTest {
    private TestHelper ts = new TestHelper();
    private List<Employee> data = new ArrayList<>(ts.getTestEmployee());


    @Test
    public void testSortingByRate() {
        Sorter sorter = new Sorter();
        sorter.sortingByRate(data);
        assertTrue (data.get(0).getRatePerHour().equals(10)
                & data.get(1).getRatePerHour().equals(80)
                & data.get(2).getRatePerHour().equals(80)
                & data.get(3).getRatePerHour().equals(80)
                & data.get(4).getRatePerHour().equals(80)
                & data.get(5).getRatePerHour().equals(90)
        );
    }

    @Test
    public void testSortingByCredentials() {
       Sorter sorter = new Sorter();
        sorter.sortingByCredentials(data);
        System.out.println(data.get(1).getRatePerHour());
        assertTrue (data.get(0).equals(ts.getTestEmployee().get(2))
                & data.get(1).equals(ts.getTestEmployee().get(3))
                & data.get(2).equals(ts.getTestEmployee().get(1))
                & data.get(3).equals(ts.getTestEmployee().get(5))
                & data.get(4).equals(ts.getTestEmployee().get(4))
                & data.get(5).equals(ts.getTestEmployee().get(0))
        );
    }
}
