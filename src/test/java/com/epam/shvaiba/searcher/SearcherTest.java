package com.epam.shvaiba.searcher;

import com.epam.shvaiba.TestHelper;
import com.epam.shvaiba.model.Employee;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;

public class SearcherTest {
    private List<Employee> testData = new TestHelper().getTestEmployee();
    private int startValue = 0;
    private int endValue = 10;

    @Test
    public void testSearchByRate() {
        Searcher searcher = new Searcher();
        List<Employee> resultList = searcher.searchByRate(testData, startValue, endValue);
        assertEquals(resultList.get(0), testData.get(0));
    }
}

