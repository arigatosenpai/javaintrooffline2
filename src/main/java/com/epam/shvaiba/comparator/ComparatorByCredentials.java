package com.epam.shvaiba.comparator;

import com.epam.shvaiba.model.Employee;

import java.util.Comparator;

public class ComparatorByCredentials implements Comparator<Employee> {
    public int compare(Employee a, Employee b){
        int i = a.getName().compareTo(b.getName());
        if (i != 0) {
            return i;
        }
        return a.getSureName().compareTo(b.getSureName());
    }
}
