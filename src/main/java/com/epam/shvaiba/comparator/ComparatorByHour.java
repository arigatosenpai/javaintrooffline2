package com.epam.shvaiba.comparator;

import com.epam.shvaiba.model.Employee;

import java.util.Comparator;

public class ComparatorByHour implements Comparator<Employee> {
    public int compare(Employee a, Employee b) {
        return a.getRatePerHour().compareTo(b.getRatePerHour());
    }
}
