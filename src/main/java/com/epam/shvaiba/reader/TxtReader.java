package com.epam.shvaiba.reader;

import com.epam.shvaiba.model.Employee;
import com.epam.shvaiba.validator.Validator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;



public class TxtReader {

    static Logger logger = LogManager.getLogger();

    public List<String[]> reading(String path) {
        List<String[]> listOfEmployees = new ArrayList<>();
        BufferedReader reader = null;
        String line;
        Validator validator = new Validator();
        try {
            reader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(path), Charset.forName("UTF-8")));

            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                if (validator.validateRow(line)){
                    listOfEmployees.add(line.split("\\s+"));
                }
                else
                {
                    logger.info("Not valid row: \"" + line + "\"");

                }
            }
        } catch (FileNotFoundException e) {
            logger.error("File not found");
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.warn("Stream can not be closed");
                }
            }
            return listOfEmployees;
        }
    }
}
