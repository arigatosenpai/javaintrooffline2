package com.epam.shvaiba.main;

import com.epam.shvaiba.enrollment.EmployeeEnrollment;
import com.epam.shvaiba.model.Employee;
import com.epam.shvaiba.project.Project;
import com.epam.shvaiba.reader.TxtReader;
import com.epam.shvaiba.searcher.Searcher;
import com.epam.shvaiba.sorter.Sorter;

import static com.epam.shvaiba.constant.Path.FILE_PATH;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String[]> employees;
        Project project = Project.getProject();

        TxtReader readerTxt = new TxtReader();
        employees = readerTxt.reading(FILE_PATH);

        //EmployeeValidatorExcess employeeValidator = new EmployeeValidatorExcess(new Validator());
        //employeesUnvalidated.stream().filter(com.epam.model.employee -> employeeValidator.validate(com.epam.model.employee)).collect(Collectors.toList())
        //employeeValidator.filter(employees);
        EmployeeEnrollment enrollment = new EmployeeEnrollment();
        enrollment.enrolling(employees, project);
        System.out.println(project.getEmployeeList().get(0).toString());
        System.out.println(project.getEmployeeList().get(1).toString());
        System.out.println(project.getEmployeeList().get(2).toString());

        Sorter sorter = new Sorter();
        sorter.sortingByRate(project.getEmployeeList());
        System.out.println("---------");
        System.out.println(project.getEmployeeList().get(0).toString());
        System.out.println(project.getEmployeeList().get(1).toString());
        System.out.println(project.getEmployeeList().get(2).toString());

        sorter.sortingByCredentials(project.getEmployeeList());
        System.out.println("---------");
        System.out.println(project.getEmployeeList().get(0).toString());
        System.out.println(project.getEmployeeList().get(1).toString());
        System.out.println(project.getEmployeeList().get(2).toString());


        List<Employee> result= Searcher.searchByRate(project.getEmployeeList(), 20, 40);
        System.out.println("---------");
        System.out.println(result.get(0).toString());

        System.out.println(project.getTeamCostByHour());
    }
}
