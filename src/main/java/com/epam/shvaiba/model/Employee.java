package com.epam.shvaiba.model;

import java.util.Objects;

public abstract class Employee {

    public Integer getRatePerHour() {
        return ratePerHour;
    }

    private Integer ratePerHour;
    private String name;
    private String sureName;
    private String engLvl;
    private String employeeType;

    public void setRatePerHour(Integer ratePerHour) {
        this.ratePerHour = ratePerHour;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSureName() {
        return sureName;
    }

    public void setSureName(String sureName) {
        this.sureName = sureName;
    }

    public String getEngLvl() {
        return engLvl;
    }

    public void setEngLvl(String engLvl) {
        this.engLvl = engLvl;
    }

    public String getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(String employeeType) {
        this.employeeType = employeeType;
    }

    public abstract void whoAmI();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return ratePerHour.equals(employee.ratePerHour) &&
                name.equals(employee.name) &&
                sureName.equals(employee.sureName) &&
                engLvl.equals(employee.engLvl) &&
                employeeType.equals(employee.employeeType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ratePerHour, name, sureName, engLvl, employeeType);
    }
}
