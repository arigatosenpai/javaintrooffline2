package com.epam.shvaiba.model;

public class Ba extends Employee {

    public Ba(String[] data){
        setEmployeeType(data[0]);
        setName(data[1]);
        setSureName(data[2]);
        setEngLvl(data[3]);
        setRatePerHour(Integer.parseInt(data[4]));
    }

    @Override
    public void whoAmI() {
        System.out.println("I'm ba specialist");
    }

    public String toString() {
        return getEmployeeType() + " " + getName() + " " + getSureName() + " " + getEngLvl() + " " + getRatePerHour();
    }
}
