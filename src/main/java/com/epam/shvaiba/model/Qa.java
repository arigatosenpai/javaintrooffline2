package com.epam.shvaiba.model;

public class Qa extends Employee {
    private String qaType;

    public Qa(String[] data){
        setEmployeeType(data[0]);
        setName(data[1]);
        setSureName(data[2]);
        setEngLvl(data[3]);
        setQaType(data[4]);
        setRatePerHour(Integer.parseInt(data[5]));
    }
    public String getQaType() {
        return qaType;
    }

    public void setQaType(String qaType) {
        this.qaType = qaType;
    }

    @Override
    public void whoAmI() {
        System.out.println("I'm qa specialist");
    }

    public String toString() {
        return getEmployeeType() + " " + getName() + " " + getSureName() + " " + getEngLvl() + " " + qaType + " " + getRatePerHour(); //
    }

}
