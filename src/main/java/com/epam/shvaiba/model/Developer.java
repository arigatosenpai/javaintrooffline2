package com.epam.shvaiba.model;

public class Developer extends Employee {
    private String progLang;

    public String getProgLang() {
        return progLang;
    }

    public void setProgLang(String progLang) {
        this.progLang = progLang;
    }

    public Developer(String[] data){
        setEmployeeType(data[0]);
        setName(data[1]);
        setSureName(data[2]);
        setEngLvl(data[3]);
        setProgLang(data[4]);
        setRatePerHour(Integer.parseInt(data[5]));
    }

    public Developer (){

    }




    @Override
    public void whoAmI() {
        System.out.println("I'm developer");
    }

    @Override
    public String toString() {
        return getEmployeeType() + " " + getName() + " " + getSureName() + " " + getEngLvl() + " " + progLang + " " + getRatePerHour();
    }
}
