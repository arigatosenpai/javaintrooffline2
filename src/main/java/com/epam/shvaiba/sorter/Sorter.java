package com.epam.shvaiba.sorter;

import com.epam.shvaiba.model.Employee;
import com.epam.shvaiba.project.Project;
import com.epam.shvaiba.comparator.ComparatorByCredentials;
import com.epam.shvaiba.comparator.ComparatorByHour;

import java.util.Collections;
import java.util.List;

public class Sorter {
    public void sortingByRate(List<Employee> list) {
        Collections.sort(list, new ComparatorByHour());
    }

    public void sortingByCredentials(List<Employee> list) {
        Collections.sort(list, new ComparatorByCredentials());
    }
}
