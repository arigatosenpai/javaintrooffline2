package com.epam.shvaiba.validator;

import java.util.regex.Pattern;

public class Validator {

    private final String regExpFinalQa = ("(?i)\\s*qA\\s+[A-Za-z]+\\s+[A-Za-z]+\\s+[A-Za-z][0-2]\\s+(Manual|automated)\\s+[0-9]{1,3}");
    private final String regExpFinalBa = ("(?i)\\s*BA\\s+[A-Za-z]+\\s+[A-Za-z]+\\s+[A-Za-z][0-2]\\s+[0-9]{1,3}");
    private final String regExpFinalDev = ("(?i)\\s*DEV\\s+[A-Za-z]+\\s+[A-Za-z]+\\s+[A-Za-z][0-2]\\s+[A-Za-z]+\\s+[0-9]{1,3}");
    private final String regExpFinal = ("((?i)\\s*qA\\s+[A-Za-z]+\\s+[A-Za-z]+\\s+[A-Za-z][0-2]\\s+(Manual|automated)\\s+[0-9]{1,3})|((?i)\\s*DEVeloper\\s+[A-Za-z]+\\s+[A-Za-z]+\\s+[A-Za-z][0-2]\\s+[A-Za-z]+\\s+[0-9]{1,3})|((?i)\\s*BA\\s+[A-Za-z]+\\s+[A-Za-z]+\\s+[A-Za-z][0-2]\\s+[0-9]{1,3})");

    public boolean validateRow(String str) {
        return Pattern.matches(regExpFinal, str);
    }
}


/*
public class Validator {
    private final String regExp = "[A-Za-z]+"; //nameSurenameProgLangqaTypeEmployeeTypeReg
    private final String nameSureNameReg = "[A-Za-z]+";
    private final String engLvlReg = "[A-Za-z][0-2]";
    private final String progLangReg = "[A-Za-z]+";
    private final String qaTypeReg = "[A-Za-z]+";
    private final String employeeTypeReg = "[A-Za-z]+";
    private final String rateReg = "[0-9]{1,3}";
    private final int qaLength = 6;
    private final int devLength = 6;
    private final int baLength = 5;
    private final String regExpFinalQa = ("(?i)\\s*qA\\s+[A-Za-z]+\\s+[A-Za-z]+\\s+[A-Za-z][0-2]\\s+(Manual|automated)\\s+[0-9]{1,3}");
    private final String regExpFinalBa = ("(?i)\\s*BA\\s+[A-Za-z]+\\s+[A-Za-z]+\\s+[A-Za-z][0-2]\\s+[0-9]{1,3}");
    private final String regExpFinalDev = ("(?i)\\s*DEV\\s+[A-Za-z]+\\s+[A-Za-z]+\\s+[A-Za-z][0-2]\\s+[A-Za-z]+\\s+[0-9]{1,3}");
    private final String regExpFinal = ("((?i)\\s*qA\\s+[A-Za-z]+\\s+[A-Za-z]+\\s+[A-Za-z][0-2]\\s+(Manual|automated)\\s+[0-9]{1,3})|((?i)\\s*DEV\\s+[A-Za-z]+\\s+[A-Za-z]+\\s+[A-Za-z][0-2]\\s+[A-Za-z]+\\s+[0-9]{1,3})|((?i)\\s*BA\\s+[A-Za-z]+\\s+[A-Za-z]+\\s+[A-Za-z][0-2]\\s+[0-9]{1,3})");

    public boolean validating(String[] employee) {
        if ((employee.length == qaLength) && employee[0].toLowerCase().equals("qa")) {
            return qaValidator(employee);
        } else if ((employee.length == devLength) && employee[0].toLowerCase().equals("developer")) {
            return devValidator(employee);
        } else if ((employee.length == baLength) && employee[0].toLowerCase().equals("ba")) {
            return baValidator(employee);
        } else {
            return false;//throw new Exception();//генерируем исключение и логгирование
        }
    }

    private boolean qaValidator(String[] employee) {

        if (Pattern.matches(regExp, employee[1])
                & Pattern.matches(regExp, employee[2])
                & Pattern.matches(engLvlReg, employee[3])
                & Pattern.matches(regExp, employee[4])
                & Pattern.matches(rateReg, employee[5])) {
            return true;
        } else {
            return false;
        }
    }

    private boolean devValidator(String[] employee) {
        if (Pattern.matches(regExp, employee[1])
                & Pattern.matches(regExp, employee[2])
                & Pattern.matches(engLvlReg, employee[3])
                & Pattern.matches(regExp, employee[4])
                & Pattern.matches(rateReg, employee[5])) {
            return true;
        } else {
            return false;
        }
    }

    private boolean baValidator(String[] employee) {
        if (Pattern.matches(nameSureNameReg, employee[1])
                & Pattern.matches(nameSureNameReg, employee[2])
                & Pattern.matches(engLvlReg, employee[3])
                & Pattern.matches(rateReg, employee[4])) {
            return true;
        } else {
            return false;
        }
    }
}
 */
