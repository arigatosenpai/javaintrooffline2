package com.epam.shvaiba.searcher;

import com.epam.shvaiba.model.Employee;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Searcher {
    public static List<Employee> searchByRate(List<Employee> list, int startValue, int endValue){
        if (list == null || list.isEmpty()) {
            return Collections.emptyList();
        }
        return list.stream()
                .filter( employee -> employee.getRatePerHour() >= startValue && employee.getRatePerHour() <= endValue )
                .collect(Collectors.toList());
    }

}
