package com.epam.shvaiba.project;

import com.epam.shvaiba.model.Employee;

import java.util.ArrayList;
import java.util.List;

public class Project {
    private static Project ourInstance;
    private List<Employee> projectList = new ArrayList<>();
    private int teamCostByHour;

    public static Project getProject() {
        if (ourInstance == null) {
            ourInstance = new Project();
        }
        return ourInstance;
    }

    private Project() {

    }

    public void addNewEmployee (Employee employee) {
        this.teamCostByHour += employee.getRatePerHour();
        projectList.add(employee);
    }

    public List<Employee> getEmployeeList(){
        return projectList;
    }

    public int getTeamCostByHour(){
        return teamCostByHour;
    }
}
