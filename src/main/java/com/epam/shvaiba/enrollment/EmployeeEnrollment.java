package com.epam.shvaiba.enrollment;

import com.epam.shvaiba.factory.EmployeeFactory;
import com.epam.shvaiba.project.Project;

import java.util.List;
import java.util.ListIterator;

public class EmployeeEnrollment {

    public void enrolling(List<String[]> validatedEmpl, Project project) {

        ListIterator<String[]> listIter = validatedEmpl.listIterator();
        EmployeeFactory employeeFactory = new EmployeeFactory();

        while (listIter.hasNext()) {
            project.addNewEmployee(
                    employeeFactory.createEmployee(listIter.next()));
        }
    }
}
