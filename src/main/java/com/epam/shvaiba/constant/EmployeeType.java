package com.epam.shvaiba.constant;

import java.lang.*;

public enum EmployeeType {
    QA("qa"),
    DEVELOPER("developer"),
    BA("ba");
    private String value;

    EmployeeType(String value){
        this.value = value;
    }

    public String getString() {
        return value;
    }
}