package com.epam.shvaiba.constant;

public enum ProgLang {
    JAVA,
    CS,
    JS,
    PYTHONS,
    RUBY,
    CPP,
    C,
    PHP
}
