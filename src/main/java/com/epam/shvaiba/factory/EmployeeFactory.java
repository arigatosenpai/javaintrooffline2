package com.epam.shvaiba.factory;

import com.epam.shvaiba.model.Ba;
import com.epam.shvaiba.constant.EmployeeType;
import com.epam.shvaiba.model.Developer;
import com.epam.shvaiba.model.Employee;
import com.epam.shvaiba.model.Qa;

public class EmployeeFactory {
    public Employee createEmployee(String[] employee) {
        if (employee[0].toLowerCase().equals(EmployeeType.QA.getString())) {
            return new Qa(employee);
        } else if (employee[0].toLowerCase().equals(EmployeeType.DEVELOPER.getString())) {
            return new Developer(employee);
        } else if (employee[0].toLowerCase().equals(EmployeeType.BA.getString())) {
            return new Ba(employee);
        } else {
            return null;
        }
    }
}
